/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inatel.stockquotemanager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author douglas
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({com.inatel.stockquotemanager.controllers.ControllersSuite.class, com.inatel.stockquotemanager.ApiErrorTest.class, com.inatel.stockquotemanager.ServerPortCustomizerTest.class, com.inatel.stockquotemanager.ApiValidationErrorTest.class, com.inatel.stockquotemanager.StockQuotesManagerApplicationTest.class, com.inatel.stockquotemanager.ApiSubErrorTest.class, com.inatel.stockquotemanager.repository.RepositorySuite.class, com.inatel.stockquotemanager.RestExceptionHandlerTest.class, com.inatel.stockquotemanager.DataConfigurationTest.class, com.inatel.stockquotemanager.utils.UtilsSuite.class, com.inatel.stockquotemanager.service.ServiceSuite.class, com.inatel.stockquotemanager.EntityNotFoundExceptionTest.class, com.inatel.stockquotemanager.models.ModelsSuite.class})
public class StockquotemanagerSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
