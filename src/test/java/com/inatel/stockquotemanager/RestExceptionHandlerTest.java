/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inatel.stockquotemanager;

import javax.persistence.EntityNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.context.request.WebRequest;

/**
 *
 * @author douglas
 */
public class RestExceptionHandlerTest {
    
    public RestExceptionHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of handleHttpMessageNotReadable method, of class RestExceptionHandler.
     */
    @Test
    public void testHandleHttpMessageNotReadable() {
        System.out.println("handleHttpMessageNotReadable");
        HttpMessageNotReadableException ex = null;
        HttpHeaders headers = null;
        HttpStatus status = null;
        WebRequest request = null;
        RestExceptionHandler instance = new RestExceptionHandler();
        ResponseEntity<Object> expResult = null;
        ResponseEntity<Object> result = instance.handleHttpMessageNotReadable(ex, headers, status, request);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleEntityNotFound method, of class RestExceptionHandler.
     */
    @Test
    public void testHandleEntityNotFound() {
        System.out.println("handleEntityNotFound");
        EntityNotFoundException ex = null;
        RestExceptionHandler instance = new RestExceptionHandler();
        ResponseEntity<Object> expResult = null;
        ResponseEntity<Object> result = instance.handleEntityNotFound(ex);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
