package com.inatel.stockquotemanager.models;

import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Quotes {   
 
  @Id
  @NotNull
  private String id;  
  //HASH MAPPING KEY = DATE AND VALUE EQUAL QUOTE
  @NotNull  
  private HashMap<String, String> quotes;
  public Quotes(){
    this.quotes = new HashMap<>();
  }
  
  public Quotes(String id, HashMap quotes){
    this.id = id;
    this.quotes = quotes;
  }
  
  public void setId(String id){
    this.id = id;
  }

  public void setQuotes(HashMap<String, String> quotes){
    this.quotes = quotes;
  }
  
  public String getId(){
    return this.id;
  }
  
  public HashMap<String, String> getQuotes(){
    return this.quotes;
  }
}