package com.inatel.stockquotemanager.models;

import javax.persistence.Entity;


public class Stock {
  private String id;
  private String description;


  public void setId(String id){
    this.id = id;
  }

  public void setDescription(String description){
    this.description = description;
  }
  
  public String getId(){
    return this.id;
  }
  
  public String getDescription(){
    return this.description;
  }

}