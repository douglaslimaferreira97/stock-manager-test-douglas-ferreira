package com.inatel.stockquotemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import com.inatel.stockquotemanager.service.StockServiceCache;

@SpringBootApplication
public class StockQuotesManagerApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		 return application.sources(StockQuotesManagerApplication.class);
	}
	public static void main(String[] args) {		
		StockServiceCache.getAllStocksCache();	
		SpringApplication app = new SpringApplication(StockQuotesManagerApplication.class);
        app.setDefaultProperties(Collections
          .singletonMap("server.port", "8081"));
        app.run(args);
		SpringApplication.run(StockQuotesManagerApplication.class, args);
	}

	@RequestMapping("/")
	public String hello() {
		return "Hello World from Tomcat";
	}

}
