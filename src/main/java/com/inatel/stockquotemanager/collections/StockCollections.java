package com.inatel.stockquotemanager.collections;

import java.util.ArrayList;
import java.util.List;


public class StockCollections {
  private static StockCollections INSTANCE;
  public static StockCollections getInstance(){
    if(INSTANCE==null){
      INSTANCE = new StockCollections();
    }
    return INSTANCE;
  }

  private StockCollections(){
    this.stocksIds = new ArrayList<String>();
  }
  private List<String> stocksIds; 

  public void addStockId(String id){
    this.stocksIds.add(id);
  }

  public void clearStockIds(){
    this.stocksIds.clear();
  }

  public List<String> getStockIds(){
    return this.stocksIds;
  }

  public boolean existsId(String id){
    return this.stocksIds.contains(id);
  }
}