package com.inatel.stockquotemanager.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

import com.inatel.stockquotemanager.EntityNotFoundException;
import com.inatel.stockquotemanager.models.Quotes;
import com.inatel.stockquotemanager.service.QuotesService;

@RestController
@RequestMapping("/stock-quotes")
public class QuotesController{
  @Autowired
  private QuotesService service;
  /**
   * 
   * @return
   */
  @GetMapping(produces = "application/json")
  public List<Quotes> getAll() {
    return service.getAll();
  }

  @PostMapping()
  public Quotes store(@RequestBody @Valid Quotes quotes) throws EntityNotFoundException  {
    return service.store(quotes);
  }

  @GetMapping("/{id}")
  public Quotes getById(@PathVariable String id) throws EntityNotFoundException  {
    return service.getById(id);
  }
  
}