package com.inatel.stockquotemanager.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;



import com.inatel.stockquotemanager.collections.StockCollections;


@RestController
@RequestMapping("/stockcache")
public class StockController { 
  @DeleteMapping()
  public void getById() {
    StockCollections.getInstance().clearStockIds();    
  }  
}