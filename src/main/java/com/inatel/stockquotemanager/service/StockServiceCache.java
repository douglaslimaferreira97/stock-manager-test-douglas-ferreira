package com.inatel.stockquotemanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.inatel.stockquotemanager.collections.StockCollections;
import com.inatel.stockquotemanager.models.Stock;
import com.inatel.stockquotemanager.utils.StockList;
@Service
public class StockServiceCache {
  @Autowired
  private static RestTemplate restTemplate = new RestTemplate();
  public static void getAllStocksCache(){    
    try{
      final String uri = "http://localhost:8080/stock";   
      StockList stocks= restTemplate.getForObject(uri, StockList.class);
      for(Stock stock : stocks){
        StockCollections.getInstance().addStockId(stock.getId());
      }
    } catch(Exception e){

    }
  
  }
}