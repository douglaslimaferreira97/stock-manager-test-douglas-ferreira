package com.inatel.stockquotemanager.service;

import java.util.List;
import java.util.Optional;

import com.inatel.stockquotemanager.EntityNotFoundException;
import com.inatel.stockquotemanager.collections.StockCollections;
import com.inatel.stockquotemanager.models.Quotes;
import com.inatel.stockquotemanager.models.Stock;
import com.inatel.stockquotemanager.repository.QuotesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class QuotesService {
  @Autowired
  private QuotesRepository quotesRepository;  

  /**
   * 
   * @return All quotes list
   */
  public List<Quotes> getAll(){
    return quotesRepository.findAll();
  }

  /**
   * Add new quote
   * @param quote
   * @return New quote saved
   */
  public Quotes store(Quotes quote){
    Boolean exists = StockCollections.getInstance().existsId(quote.getId());
    if(!exists){
      throw new EntityNotFoundException(Stock.class, "id", quote.getId());    
    }
    return quotesRepository.saveAndFlush(quote);
  }
  public Quotes getById(String id){
    Boolean exists = StockCollections.getInstance().existsId(id);
    if(!exists){
      throw new EntityNotFoundException(Stock.class, "id", id);    
    }
    return quotesRepository.findById(id).get();
  }


}