package com.inatel.stockquotemanager.repository;

import com.inatel.stockquotemanager.models.Quotes;

import org.springframework.data.jpa.repository.JpaRepository;

public interface QuotesRepository extends JpaRepository<Quotes, String>{
  
}